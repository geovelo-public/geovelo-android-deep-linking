package fr.geovelo.deeplinking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        findViewById(R.id.btnLaunchAppOrStore)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchAppOrStore();
                    }
                });

        findViewById(R.id.btnLaunchAppOrWebsite)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchAppOrWebsite();
                    }
                });


        findViewById(R.id.btnLaunchFavorite)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchUrl("https://www.geovelo.app/favorites");
                    }
                });

        findViewById(R.id.btnAddressUsingCoordinates)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchUrl("https://www.geovelo.app/address?location=47.4018684,0.6872403");
                    }
                });

        findViewById(R.id.btnAddressUsingCoordinatesWithTitle)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchUrl("https://www.geovelo.app/address?"+"location=47.4018684,0.6872403," + Uri.encode("Some address 37000 Tours"));
                    }
                });

        findViewById(R.id.btnItinerariesUsingOneLocation)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchUrl("https://www.geovelo.app/itineraries?"+"location=47.4018684,0.6872403," + Uri.encode("Some arrival, 37000 Tours"));
                    }
                });

        findViewById(R.id.btnItinerariesUsingTwoLocation)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchUrl("https://www.geovelo.app/itineraries?"+"location[]=47.4018684,0.6872403," + Uri.encode("Some departure, 37000 Tours") + "&location[]=47.4118684,0.6972403," + Uri.encode("Some arrival, 37000 Tours"));
                    }
                });

        findViewById(R.id.btnUrlFrom)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchUrl("https://www.geovelo.app/route?from=0.6872403,47.4018684");
                    }
                });
        findViewById(R.id.btnUrlFromTo)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchUrl("https://www.geovelo.app/route?from=0.6872403,47.4018684&to=0.693512,47.421583");
                    }
                });


        findViewById(R.id.btnGeoLatLon)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchGeoIntent("47.4018684,0.6872403");
                    }
                });
        findViewById(R.id.btnGeoQueryLatLon)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchGeoIntent("0,0?q=47.4018684,0.6872403");
                    }
                });
        findViewById(R.id.btnGeoQueryString)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchGeoIntent("0,0?q=" + Uri.encode("Some departure, 37000 Tours"));
                    }
                });

        findViewById(R.id.btnComputedRoute)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchUrl("https://geovelo.app/fr/computed-route/bG9jPTQ3LjM4OTA4MywwLjY4NzM2OCZsb2M9NDcuMzg5ODgxLDAuNjg2NjMyI01FRElBTiNGYWxzZSNNRURJQU4jMjAjRmFsc2UjTm9uZSMyMDIzLTA1LTAzIDE0OjM1OjU5LjY1MTgzNSNUUkFESVRJT05BTCMwIzAjUkVDT01NRU5ERUQjRmFsc2UjVHJ1ZQ==");
                    }
                });
    }

    protected void launchGeoIntent(String params) {
        Uri intentUri = Uri.parse("geo:" + params);
        Intent intent = new Intent();
        intent.setData(intentUri);
        intent = getPackageManager().getLaunchIntentForPackage("fr.geovelo");

        startActivity(intent);
    }

    protected void launchIntent(String schemePart, String params) {
        Uri intentUri = Uri.parse("geovelo." + schemePart + ":" + params);
        Intent intent = getPackageManager().getLaunchIntentForPackage("fr.geovelo");
        intent.setData(intentUri);
        startActivity(intent);
    }

    public void launchAppOrStore() {
        String geoveloPackageName = "fr.geovelo";
        try {
            // Launch the Geovelo app when installed
            getPackageManager().getPackageInfo(geoveloPackageName, 0);

            Intent intent = getPackageManager().getLaunchIntentForPackage(geoveloPackageName);
            startActivity(intent);

        } catch (PackageManager.NameNotFoundException e) {
            // Launch the play store when not installed
            launchUrl("market://details?id=" + geoveloPackageName);
        }
    }

    public void launchAppOrWebsite() {
        String geoveloPackageName = "fr.geovelo";
        try {
            // Launch the Geovelo app when installed
            getPackageManager().getPackageInfo(geoveloPackageName, 0);

            Intent intent = getPackageManager().getLaunchIntentForPackage(geoveloPackageName);
            startActivity(intent);

        } catch (PackageManager.NameNotFoundException e) {
            // Launch the play store when not installed
            launchUrl("https://www.geovelo.app");
        }
    }

    public void launchUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
