## Geovelo - Deep Linking

### Android

#### Using URLs (recommended way)


_Switch to tab "profile", showing the current user statistics or a view inviting to create an account if user is unlogged_

    launchUrl("https://www.geovelo.app/user/stats");

_Show the favorites screen or invite the user to create an account if unlogged_

    launchUrl("https://www.geovelo.app/favorites");

_Show the view to generate a loop_

    launchUrl("https://www.geovelo.app/loop");

_Shows the details of a ride (20 is the ID of the ride to show, replace it according to your needs)_

    launchUrl("https://www.geovelo.app/rides/20");

_Shows the FlyOver for a route passing by the given waypoints (at least one, in this case, current position is used as starting point)_

    launchUrl("https://www.geovelo.app/itineraries?"+"location=47.4018684,0.6872403," + Uri.encode("Some arrival, 37000 Tours"));


_Shows the list of communities joined by the current user or a view inviting to create an account_

    launchUrl("https://www.geovelo.app/communities");

_Shortcut to join a private community, shows an invite to create an account if unlogged_

    launchUrl("https://www.geovelo.app/communities/invites/IQCNA");

_Shows the details for an already computed route_

    launchUrl("https://www.geovelo.app/computed-route/bG9jPTQ3LjM2NDQwODcyMTc1OTc0NCwwLjY1NTQwMzEzNzIwNzAzMTQmbG9jPTQ3LjQxMTM2MTY2ODUzOTE3LDAuNjk1OTE1MjIyMTY3OTY4OSNNRURJQU4jRmFsc2UjTUVESUFOIzIxI0ZhbHNlI05vbmUjMjAyMC0wOS0wNCAxNTozNzoxNi4zNzQ0MzcjVFJBRElUSU9OQUwjMCMwI1JFQ09NTUVOREVEI0ZhbHNlI1RydWU=)

OR

    launchUrl("https://www.geovelo.app/route/bG9jPTQ3LjM2NDQwODcyMTc1OTc0NCwwLjY1NTQwMzEzNzIwNzAzMTQmbG9jPTQ3LjQxMTM2MTY2ODUzOTE3LDAuNjk1OTE1MjIyMTY3OTY4OSNNRURJQU4jRmFsc2UjTUVESUFOIzIxI0ZhbHNlI05vbmUjMjAyMC0wOS0wNCAxNTozNzoxNi4zNzQ0MzcjVFJBRElUSU9OQUwjMCMwI1JFQ09NTUVOREVEI0ZhbHNlI1RydWU=)


_Same as itineraries but parameters differs (from and to must be provided steps is optionnal)_

    launchUrl("https://www.geovelo.app/route?from=47.10347,5.480743&to=47.09222,5.48972)
    
    launchUrl("https://www.geovelo.app/route?from=-1.742661,43.376965&steps=-1.786272,43.370858;-1.791611,43.366993&to=-1.792176,43.391535&z=13.58&zone=bayonne)


#### Using native intent

##### Launch the app OR the store when app is not installed

_You can test this in the sample app_

    String geoveloPackageName = "fr.geovelo";
    try {
        // Launch the Geovelo app when installed
        getPackageManager().getPackageInfo(geoveloPackageName, 0);

        Intent intent = getPackageManager().getLaunchIntentForPackage(geoveloPackageName);
        startActivity(intent);

    } catch (PackageManager.NameNotFoundException e) {

        // Launch the play store when not installed
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + geoveloPackageName));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


### iOS

[See the project sample for iOS](https://gitlab.com/geovelo-public/geovelo-ios-deep-linking)

